package com.test.marveldemo.data

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Movies (

    @PrimaryKey val id: Long,
    val original_title: String,
    val poster_path: String,
    val vote_average: Float

) : Parcelable