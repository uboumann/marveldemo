package com.test.marveldemo.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Return (
    val page: Long,
    val total_results: String,
    val total_pages: String,
    val results: List<Movies>
) : Parcelable