package com.test.marveldemo.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieDetails(
    val id: Long,
    val name: String,
    val background_image: String,
    val description: String
) : Parcelable