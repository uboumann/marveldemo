package com.test.marveldemo.view.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import com.test.marveldemo.base.LiveCoroutinesViewModel
import com.test.marveldemo.data.Movies
import com.test.marveldemo.repository.MainRepository

class MainViewModel constructor(
    private val mainRepository: MainRepository
) : LiveCoroutinesViewModel() {

    private var moviesFetchingLiveData: MutableLiveData<Boolean> = MutableLiveData()

    val moviesList: LiveData<List<Movies>>
    val toastLiveData: MutableLiveData<String> = MutableLiveData()


    init {
        Log.e("teste", "teste")
        this.moviesList = this.moviesFetchingLiveData.switchMap {
            launchOnViewModelScope {
                this.mainRepository.loadGames { this.toastLiveData.postValue(it) }
            }
        }
    }

    fun fetchMoviesList() = this.moviesFetchingLiveData.postValue(true)

}