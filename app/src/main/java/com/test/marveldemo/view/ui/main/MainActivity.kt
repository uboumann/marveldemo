package com.test.marveldemo.view.ui.main

import android.os.Bundle
import android.util.Log
import com.skydoves.transformationlayout.onTransformationStartContainer
import com.test.marveldemo.R
import com.test.marveldemo.base.DataBindingActivity
import com.test.marveldemo.databinding.ActivityMainBinding
import org.koin.android.viewmodel.ext.android.getViewModel



class MainActivity : DataBindingActivity() {

    private val binding: ActivityMainBinding by binding(R.layout.activity_main)


    override fun onCreate(savedInstanceState: Bundle?) {
        onTransformationStartContainer()
        super.onCreate(savedInstanceState)
        binding.apply {
            lifecycleOwner = this@MainActivity
            vm = getViewModel<MainViewModel>().apply { fetchMoviesList() }

            Log.e("test", "test")
        }
    }
}
