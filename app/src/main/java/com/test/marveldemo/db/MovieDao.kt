package com.test.marveldemo.db

import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.test.marveldemo.data.Movies

interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPosterList(posters: List<Movies>)

    @Query("SELECT * FROM Movies WHERE id = :id_")
    fun getPoster(id_: Long): Movies

    @Query("SELECT * FROM Movies")
    fun getPosterList(): List<Movies>
}