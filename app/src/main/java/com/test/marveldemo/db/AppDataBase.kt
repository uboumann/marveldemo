package com.test.marveldemo.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.test.marveldemo.data.Movies

@Database(entities = [Movies::class], version = 1, exportSchema = false)
//@TypeConverters(value = [IntegerListConverter::class])
abstract class AppDatabase : RoomDatabase() {

    abstract fun movieDao(): MovieDao
}