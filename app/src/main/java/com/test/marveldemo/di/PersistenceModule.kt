package com.test.marveldemo.di

import androidx.room.Room
import com.test.marveldemo.R
import com.test.marveldemo.db.AppDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val persistenceModule = module {

    single {
        Room
            .databaseBuilder(androidApplication(), AppDatabase::class.java,
                androidApplication().getString(R.string.app_name))
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
    }

    single { get<AppDatabase>().movieDao() }
}