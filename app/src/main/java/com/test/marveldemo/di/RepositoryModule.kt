package com.test.marveldemo.di

import com.test.marveldemo.repository.MainRepository
import org.koin.dsl.module

val repositoryModule = module {

    single { MainRepository(get(), get()) }
}