@file:Suppress("unused")

package com.test.marveldemo

import android.app.Application
import com.test.marveldemo.di.networkModule
import com.test.marveldemo.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MyApplication)
            modules(networkModule)
            modules(viewModelModule)
        }
    }

}