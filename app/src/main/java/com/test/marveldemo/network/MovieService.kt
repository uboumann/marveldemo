package com.test.marveldemo.network

import com.test.marveldemo.data.Return
import retrofit2.Call
import retrofit2.http.GET

interface MovieService {

@GET("popular?api_key=e0b089f8da86a3f60990523dee846bc4&language=en-uk&page=1")
fun fetchGamesList(): Call<Return>

}