package com.test.marveldemo.network

import com.test.marveldemo.data.Movies

class MovieClient(private val movieService: MovieService) {

    fun fetchMovies(
        onResult: (response: ApiResponse<List<Movies>>) -> Unit
    ) {
        this.movieService.fetchGamesList().convert{onResult}
    }
}