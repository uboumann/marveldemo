package com.test.marveldemo.repository

interface Repository {
    var isLoading: Boolean
}