package com.test.marveldemo.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.test.marveldemo.data.Movies
import com.test.marveldemo.db.MovieDao
import com.test.marveldemo.network.MovieClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MainRepository constructor(
    private val movieClient: MovieClient,
    private val movieDao: MovieDao
) : Repository {

    override var isLoading: Boolean = false

    init {

    }

    suspend fun loadGames(error: (String) -> Unit) = withContext(Dispatchers.IO) {
        Log.e("load", "ok")
        val liveData = MutableLiveData<List<Movies>>()
        var games = movieDao.getPosterList()
        if (games.isEmpty()) {
            isLoading = true


        }
        liveData.apply { postValue(games) }
    }

}